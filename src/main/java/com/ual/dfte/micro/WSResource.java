package com.ual.dfte.micro;

import java.util.Optional;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.config.inject.ConfigProperty;

@Path("/ws")
public class WSResource {
    @ConfigProperty(name = "greeting.message")
    private String message;
    
    @ConfigProperty(name = "greeting.suffix", defaultValue="!")
    private String suffix;
    
    @ConfigProperty(name = "greeting.name")
    private Optional<String> name;

    @Inject
    WSService service;

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String hello() {
        return message + " " + name.orElse("world") + suffix;
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/greeting/{name}")
    public String greeting(@PathParam("name") String name) {
        return service.greeting(name);
    }
}
