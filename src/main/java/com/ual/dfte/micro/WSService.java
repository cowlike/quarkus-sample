package com.ual.dfte.micro;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class WSService {

    public String greeting(String name) {
        return "hello " + name;
    }

}
