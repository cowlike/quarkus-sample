package com.ual.dfte.micro;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import org.eclipse.microprofile.config.Config;
import org.eclipse.microprofile.config.ConfigProvider;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
public class WSResourceTest {

    @Test
    public void testHelloEndpoint() {
        Config cfg = ConfigProvider.getConfig();
        given()
          .when().get("/ws")
          .then()
             .statusCode(200)
             .body(is("hello " + cfg.getValue("greeting.name", String.class) + "!"));
    }

    @Test
    public void testGreetingEndpoint() {
        String uuid = UUID.randomUUID().toString();
        given()
          .pathParam("name", uuid)
          .when().get("/ws/greeting/{name}")
          .then()
            .statusCode(200)
            .body(is("hello " + uuid));
    }

}
