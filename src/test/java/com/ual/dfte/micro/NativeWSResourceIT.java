package com.ual.dfte.micro;

import io.quarkus.test.junit.SubstrateTest;

@SubstrateTest
public class NativeWSResourceIT extends WSResourceTest {

    // Execute the same tests but in native mode.
}